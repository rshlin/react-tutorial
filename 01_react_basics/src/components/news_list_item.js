import React from 'react';
import classes from '../css/styles.css';

const NewsItem = ({news}) =>{

  return(
    <div className={classes.news_item}>
      <h3>
        {news.title}
      </h3>
      <div>
        {news.feed}
      </div>
    </div>
  )
}

export default NewsItem
