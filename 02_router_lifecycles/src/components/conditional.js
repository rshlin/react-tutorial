import React from 'react';
import ReactDOM from 'react-dom';

const Conditional = ()=>{

  const value = true;

  return (
    <div>
      { value ? 
        <div>Hello, it is true</div> : null }
    </div>
  )
}

export default Conditional;

